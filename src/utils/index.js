import Vue from 'vue'

export function range(n){
  let out = []
  for(let i = 0; i < n; i++){
    out.push(i)
  }
  return out
}

export function sleep(t){
  return new Promise(r => setTimeout(r, t))
}

export function nextFrame(){
  return new Promise(r => requestAnimationFrame(r))
}

export function nextTick(){
  return new Promise(r => Vue.nextTick(r))
}

export function randint(a, b){
  if (b) {
    b = b + 1
    return Math.floor(a + Math.random() * (b - a))
  }else if(a){
    a = a + 1
    return Math.floor(Math.random() * a)
  }else{
    console.error('randint: 错误的函数调用')
  }
}