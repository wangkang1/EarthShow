import Vue from 'vue'
import VueRouter from 'vue-router'
import Detail from './components/Detail'
import Home from './components/Home'
import Topic from './components/Topic'
import Person from './components/Person'
import EventList from './components/EventList'
import PersonList from './components/PersonList'

Vue.use(VueRouter)

var router = new VueRouter({
  routes: [{
    path: '/',
    redirect: '/home'
  },{
    path: '/home',
    component: Home
  },{
    path: '/topic',
    component: Topic
  },{
    path: '/person',
    component: Person
  },{
    path: '/detail',
    component: Detail
  },{
    path: '/eventList',
    component: EventList
  },{
    path: '/personlist',
    component: PersonList
  }]
})

export default router
